import 'package:flutter/material.dart';

const APP_NAME = 'Deta Service';
const APP_NAME_TAG_LINE = 'On-Demand Home Services App';
const DEFAULT_LANGUAGE = 'en';

const DOMAIN_URL = 'https://www.detadev.com';
const BASE_URL = '$DOMAIN_URL/api/';

// You can change this to your provider package name
const PROVIDER_PACKAGE_NAME = 'com.detaprovider.uas';
const IOS_LINK_FOR_PARTNER =
    "https://apps.apple.com/in/app/handyman-provider-app/id1596025324";

var defaultPrimaryColor = Color(0xFF5F60B9);
const DASHBOARD_AUTO_SLIDER_SECOND = 5;

const ONESIGNAL_APP_ID = '1ce7e8da-2ec2-4215-992e-162ffc78fea3';
const ONESIGNAL_REST_KEY = "M2MzMjE2MGEtMTkzMC00MGUxLWE1NWUtMWZlNDdiYzMxOGRj";
const ONESIGNAL_CHANNEL_ID = "d8339d58-d4c5-4ff9-bf29-b2794cc328ce";

const TERMS_CONDITION_URL = 'https://gitlab.com/Putnug1122';
const PRIVACY_POLICY_URL = 'https://gitlab.com/Putnug1122';
const HELP_SUPPORT_URL = 'https://gitlab.com/Putnug1122';

const STRIPE_MERCHANT_COUNTRY_CODE = 'US';
const STRIPE_CURRENCY_CODE = 'USD';

DateTime todayDate = DateTime(2022, 8, 24);
